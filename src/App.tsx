import * as React from 'react';
import { Helmet } from 'react-helmet';
import { BrowserRouter as Router, Redirect, Route, Switch, useLocation, useParams } from 'react-router-dom';
import GenericNotFound from './Components/GenericNotFound';
import Header from './Components/Header';
import Home from './Components/Home';
import MobileNav from './Components/MobileNav';
import Sidebar from './Components/Sidebar';

const RedirectBlogPost = () => {
    const { slug } = useParams();
    window.location.href = `https://ctprods.cyprientaque.com/blog/${slug}`;
    return <div />;
}

const RedirectBlog = () => {
    const location = useLocation();
    const query = new URLSearchParams(location.search);
    if (query.get('s')) {
        window.location.href = `https://ctprods.cyprientaque.com/blog/${query.get('s')}`;
    } else {
        window.location.href = `https://ctprods.cyprientaque.com/blog`;
    }
    return <div />;
}

export const Routes = () => {
    return (
        <Switch>
            <Route exact={true} path="/" component={Home} />
            <Route path="/experiences">
                <Redirect to="/" />
            </Route>
            <Route path="/studies">
                <Redirect to="/" />
            </Route>
            <Route path="/skills">
                <Redirect to="/" />
            </Route>
            <Route path="/hobbies">
                <Redirect to="/" />
            </Route>
            <Route path="/contact">
                <Redirect to="/" />
            </Route>
            <Route path="/portfolio">
                <Redirect to="/" />
            </Route>
            <Route exact={true} path="/blog/:slug">
                <RedirectBlogPost />
            </Route>
            <Route path="/blog">
                <RedirectBlog />
            </Route>
            <Route component={GenericNotFound} />
        </Switch>
    );
}

const App = () => {
    return (
        <Router>
            <div className="AppWrapper" id="outer-container" >
                <Helmet>
                    <title>Cv de Cyprien Taque </title>
                    <meta name="description" content="Cv interactif de Cyprien Taque. Ici vous trouverez de l'informatique, de la CAO et du travail du bois" />
                </Helmet>
                <Sidebar />
                <Header />
                <main id="pageWrap">
                    <div className="PageWrap">
                        <MobileNav />
                        <div>
                            <main>
                                <Routes />
                            </main>
                        </div>
                    </div>
                </main>
            </div>
        </Router>
    );
};
export default App;
